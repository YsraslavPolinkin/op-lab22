#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define ARRAY_SIZE 10

int main() {
	int mas[ARRAY_SIZE];


	srand(time(NULL));
	printf("Yaroslav Polinkin\n");

	for (int i = 0; i < ARRAY_SIZE; i++) {
		mas[i] = rand() % 21 - 10;
	}


	for (int i = 2; i < ARRAY_SIZE; i += 3) {
		mas[i] = 0;
	}


	for (int i = 1; i < ARRAY_SIZE - 1; i++) {
		if (mas[i] < 0) {
			mas[i + 1] = mas[i - 1] + 1;
		}
	}


	for (int i = 0; i < ARRAY_SIZE; i++) {
		printf("%d ", mas[i]);
	}

	return 0;
}